<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1175b1e407b1a6b734acd86b0b08eeb0
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'TestApp\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'TestApp\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1175b1e407b1a6b734acd86b0b08eeb0::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1175b1e407b1a6b734acd86b0b08eeb0::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit1175b1e407b1a6b734acd86b0b08eeb0::$classMap;

        }, null, ClassLoader::class);
    }
}
